class Contact:
    def __init__(self, surname, name, age, mob_phone, email):
        self.surname = surname
        self.name = name
        self.age = age
        self.mob_phone = mob_phone
        self.email = email

    def get_contact(self):
        return f"{self.surname} {self.name}, age: {self.age}, phone: {self.mob_phone}, email: {self.email}"

    def sent_message(self, message):
        print(f"Message '{message}' has been sent to {self.surname} {self.name}")


class UpdateContact(Contact):
    def __init__(self, surname, name, age, mob_phone, email, job):
        super().__init__(surname, name, age, mob_phone, email)
        self.job = job

    def get_message(self, message):
        print(f"{self.surname} {self.name} says: '{message}'")


if __name__ == "__main__":
    contact1 = Contact("Connor", "John", 30, "+123456789", "john.connor@gmail.com")
    update_contact1 = UpdateContact(
        "Smith", "Jane", 25, "+987654321", "jane.smith@gmail.com", "Developer"
    )

    print("Contact class information:")
    print("Attributes:", Contact.__dict__)
    print("Base class:", Contact.__base__)
    print("Base classes:", Contact.__bases__)

    print("\nUpdateContact class information:")
    print("Attributes:", UpdateContact.__dict__)
    print("Base class:", UpdateContact.__base__)
    print("Base classes:", UpdateContact.__bases__)

    print("\nInstance of Contact class information:")
    print("Attributes:", contact1.__dict__)
    print("Is instance of Contact:", isinstance(contact1, Contact))
    print("Is subclass of Contact:", issubclass(Contact, UpdateContact))

    print("\nInstance of UpdateContact class information:")
    print("Attributes:", update_contact1.__dict__)
    print("Is instance of UpdateContact:", isinstance(update_contact1, UpdateContact))
    print("Is subclass of Contact:", issubclass(UpdateContact, Contact))

    print("\nDeleting the 'job' attribute...")
    delattr(update_contact1, "job")

    print("\nInstance of UpdateContact class after deleting the 'job' attribute:")
    print("Attributes:", update_contact1.__dict__)
