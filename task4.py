class Contact:
    def __init__(self, surname, name, age, mob_phone, email):
        self.surname = surname
        self.name = name
        self.age = age
        self.mob_phone = mob_phone
        self.email = email

    def get_contact(self):
        return f"{self.surname} {self.name}, age: {self.age}, phone: {self.mob_phone}, email: {self.email}"

    def sent_message(self, message):
        print(f"Message '{message}' has been sent to {self.surname} {self.name}")


class UpdateContact(Contact):
    def __init__(self, surname, name, age, mob_phone, email, job):
        super().__init__(surname, name, age, mob_phone, email)
        self.job = job

    def get_message(self, message):
        print(f"{self.surname} {self.name} says: '{message}'")


if __name__ == "__main__":
    contact1 = Contact("Connor", "John", 30, "+123456789", "john.connor@gmail.com")
    update_contact1 = UpdateContact(
        "Smith", "Jane", 25, "+987654321", "jane.smith@gmail.com", "Developer"
    )

    contact2 = Contact("Jordan", "Alice", 35, "+1122334455", "alice.doe@gmail.com")
    update_contact2 = UpdateContact(
        "Johnson", "Bob", 40, "+9988776655", "bob.johnson@gmail.com", "Manager"
    )

    print("Is contact1 an instance of Contact class?", isinstance(contact1, Contact))
    print(
        "Is update_contact1 an instance of Contact class?",
        isinstance(update_contact1, Contact),
    )
    print(
        "Is contact1 an instance of UpdateContact class?",
        isinstance(contact1, UpdateContact),
    )
    print(
        "Is update_contact1 an instance of UpdateContact class?",
        isinstance(update_contact1, UpdateContact),
    )

    print("Is contact2 an instance of Contact class?", isinstance(contact2, Contact))
    print(
        "Is update_contact2 an instance of Contact class?",
        isinstance(update_contact2, Contact),
    )
    print(
        "Is contact2 an instance of UpdateContact class?",
        isinstance(contact2, UpdateContact),
    )
    print(
        "Is update_contact2 an instance of UpdateContact class?",
        isinstance(update_contact2, UpdateContact),
    )

    print("Is UpdateContact a subclass of Contact?", issubclass(UpdateContact, Contact))
