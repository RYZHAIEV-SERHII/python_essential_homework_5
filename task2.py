class Contact:
    def __init__(self, surname, name, age, mob_phone, email):
        self.surname = surname
        self.name = name
        self.age = age
        self.mob_phone = mob_phone
        self.email = email

    def get_contact(self):
        return f"{self.surname} {self.name}, age: {self.age}, phone: {self.mob_phone}, email: {self.email}"

    def sent_message(self, message):
        print(f"Message '{message}' has been sent to {self.surname} {self.name}")


class UpdateContact(Contact):
    def __init__(self, surname, name, age, mob_phone, email, job):
        super().__init__(surname, name, age, mob_phone, email)
        self.job = job

    def get_message(self, message):
        print(f"{self.surname} {self.name} says: '{message}'")


if __name__ == "__main__":
    contact1 = Contact("Connor", "John", 30, "+123456789", "john.connor@gmail.com")
    update_contact1 = UpdateContact(
        "Smith", "Jane", 25, "+987654321", "jane.smith@gmail.com", "Developer"
    )

    print("Contact 1:", contact1.get_contact())
    print("Update Contact 1:", update_contact1.get_contact())
    print("Update Contact 1 job:", update_contact1.job)

    contact1.sent_message("Hello, John!")
    update_contact1.sent_message("Hello, Jane!")
    update_contact1.get_message("How are you?")

    print("\nInvestigation of Contact class:")
    print("Contact 1 __dict__:", contact1.__dict__)
    print("Contact __base__:", Contact.__base__)
    print("Contact __bases__:", Contact.__bases__)

    print("\nInvestigation of UpdateContact class:")
    print("Update Contact 1 __dict__:", update_contact1.__dict__)
    print("Update Contact __base__:", UpdateContact.__base__)
    print("Update Contact __bases__:", UpdateContact.__bases__)
