class Contact:
    def __init__(self, surname, name, age, mob_phone, email):
        self.surname = surname
        self.name = name
        self.age = age
        self.mob_phone = mob_phone
        self.email = email

    def get_contact(self):
        return f"{self.surname} {self.name}, age: {self.age}, phone: {self.mob_phone}, email: {self.email}"

    def sent_message(self, message):
        print(f"Message '{message}' has been sent to {self.surname} {self.name}")


class UpdateContact(Contact):
    def __init__(self, surname, name, age, mob_phone, email, job):
        super().__init__(surname, name, age, mob_phone, email)
        self.job = job

    def get_message(self, message):
        print(f"{self.surname} {self.name} says: '{message}'")


if __name__ == "__main__":
    contact1 = Contact("Connor", "John", 30, "+123456789", "john.connor@gmail.com")
    update_contact1 = UpdateContact(
        "Smith", "Jane", 25, "+987654321", "jane.smith@gmail.com", "Developer"
    )

    for attr in ["surname", "name", "age", "mob_phone", "email"]:
        print(
            f"\nUsing hasattr(), getattr(), setattr(), and delattr() on attribute '{attr}':"
        )
        if hasattr(contact1, attr):
            print(f"Attribute '{attr}' exists in contact1")
            print(f"Value of '{attr}': {getattr(contact1, attr)}")
            setattr(contact1, attr, "New_Value")
            print(f"Updated value of '{attr}': {getattr(contact1, attr)}")
            delattr(contact1, attr)
            if not hasattr(contact1, attr):
                print(f"Attribute '{attr}' has been deleted from contact1")
        else:
            print(f"Attribute '{attr}' does not exist in contact1")

    for attr in ["surname", "name", "age", "mob_phone", "email", "job"]:
        print(
            f"\nUsing hasattr(), getattr(), setattr(), and delattr() on attribute '{attr}' of update_contact1:"
        )
        if hasattr(update_contact1, attr):
            print(f"Attribute '{attr}' exists in update_contact1")
            print(f"Value of '{attr}': {getattr(update_contact1, attr)}")
            setattr(update_contact1, attr, "New_Value")
            print(f"Updated value of '{attr}': {getattr(update_contact1, attr)}")
            delattr(update_contact1, attr)
            if not hasattr(update_contact1, attr):
                print(f"Attribute '{attr}' has been deleted from update_contact1")
        else:
            print(f"Attribute '{attr}' does not exist in update_contact1")
