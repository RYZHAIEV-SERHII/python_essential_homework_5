class Contact:
    def __init__(self, surname, name, age, mob_phone, email):
        self.surname = surname
        self.name = name
        self.age = age
        self.mob_phone = mob_phone
        self.email = email

    def get_contact(self):
        return f"{self.surname} {self.name}, age: {self.age}, phone: {self.mob_phone}, email: {self.email}"

    def sent_message(self, message):
        print(f"Message '{message}' has been sent to {self.surname} {self.name}")


class UpdateContact(Contact):
    def __init__(self, surname, name, age, mob_phone, email, job):
        super().__init__(surname, name, age, mob_phone, email)
        self.job = job

    def get_message(self, message):
        print(f"{self.surname} {self.name} says: '{message}'")


if __name__ == "__main__":
    contact_methods = [
        method for method in dir(Contact) if callable(getattr(Contact, method))
    ]
    print("Methods in Contact class:", contact_methods)

    update_contact_methods = [
        method
        for method in dir(UpdateContact)
        if callable(getattr(UpdateContact, method))
    ]
    print("\nMethods in UpdateContact class:", update_contact_methods)
